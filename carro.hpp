#include <iostream>
#include <string>

using namespace std;

class Carro {

private:    
        // Atributos
    string chassis;
    string cor;
    int numero_de_portas;
    string modelo;
    string fabricante;
    string estado;
    float velocidade;
    float fator_de_frenagem;
    
public:
    // Métodos
    Carro(); // Método Construtor (mesmo nome da classe)
    ~Carro(); // Método Destrutor (~nome da classe)
    // Métodos Acessores
        string get_chassis();
        void set_chassis(string chassis);

        string get_cor();
        void set_cor(string cor);

        int get_numero_de_portas();
        void set_numero_de_portas(int numero_de_portas);

        string get_modelo();
        void set_modelo(string modelo);

        string get_fabricante();
        void set_fabricante(string fabricante);

        string get_estado();
        void set_estado(string estado);

        float get_velocidade();
        void set_velocidade(float velocidade);

        
        // Outros Métodos
        void ligar();
        void desligar();
        void acelerar(float fator_de_aceleracao);
        void frear (float fator_de_frenagem);
};