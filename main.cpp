#include <iostream>
#include "carro.hpp"

int main (int argc,char ** argv){

	Carro carro1;
	Carro carro2;
	Caro *carro3;

	carro3 = new Carro();

	carro1.set_chassis("099UY6387");
	carro1.set_fabricante("Fiat");
	carro1.set_modelo("Palio");
	carro1.set_numero_de_portas(4);
	carro1.set_cor("Prata");

	carro2.set_chassis("89UI8690");
	carro2.set_fabricante("Ford");
	carro2.set_modelo("Ka");
	carro2.set_numero_de_portas(2);
	carro2.set_cor("Preto");

	carro3->set_chassis("89UI8690");
	carro3->set_fabricante("Ford");
	carro3->set_modelo("Ka");
	carro3->set_numero_de_portas(2);
	carro3->set_cor("Preto");



		cout << "Carro 1: " << endl;

		cout << " Chassis do carro: "<< carro1.get_chassis() << endl;
		cout << " Fabricante do carro: " << carro1.get_fabricante() << endl;
		cout << " Modelo do carro: " <<  carro1.get_modelo() << endl;
		cout << " Numero de portas: "<< carro1.get_numero_de_portas() << endl;
		cout << " Cor: "<< carro1.get_cor() << endl;


		cout << "Carro 2: " << endl;

		cout << " Chassis do carro: "<< carro2.get_chassis() << endl;
		cout << " Fabricante do carro: " << carro2.get_fabricante() << endl;
		cout << " Modelo do carro: " <<  carro2.get_modelo() << endl;
		cout << " Numero de portas: "<< carro2.get_numero_de_portas() << endl;
		cout << " Cor: "<< carro1.get_cor() << endl;

delete carro3;

	return 0;

};